package com.atguigu.mybatis_plus;

import com.atguigu.mybatis_plus.entity.User;
import com.atguigu.mybatis_plus.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

/**
 * @author helen
 * @since 2020/3/31
 */
@SpringBootTest
public class QueryWrapperTests {

    @Autowired
    private UserMapper userMapper;

    /**
     * 功能描述：ge、gt、le、lt、isNull、isNotNull 测试
     *
     * @author cakin
     * @date 2020/11/15
     */
    @Test
    public void testDelete() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper
                .isNull("name") // 为Null
                .ge("age", 12) // 大于等于
                .isNotNull("email"); // 不为Null
        int result = userMapper.delete(queryWrapper);
        System.out.println("删除了" + result + "行");
    }

    /**
     * 功能描述：eq、ne 测试
     *
     * @author cakin
     * @date 2020/11/15
     */
    @Test
    public void testSelectOne() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", "Tom");
        User user = userMapper.selectOne(queryWrapper); // 只能返回一条记录，多余一条则抛出异常
        System.out.println(user);
    }

    /**
     * 功能描述：between、notBetween 测试
     *
     * @author cakin
     * @date 2020/11/15
     */
    @Test
    public void testSelectCount() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.between("age", 20, 30);
        Integer count = userMapper.selectCount(queryWrapper); // 返回数据数量
        System.out.println(count);
    }

    /**
     * 功能描述：like、notLike、likeLeft、likeRight 测试
     *
     * @author cakin
     * @date 2020/11/15
     */
    @Test
    public void testSelectMaps() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .select("name", "age")
                .like("name", "e")
                .likeRight("email", "5");
        List<Map<String, Object>> maps = userMapper.selectMaps(queryWrapper); // 返回值是Map列表
        maps.forEach(System.out::println);
    }

    /**
     * 功能描述：in、notIn、inSql、notinSql、exists、notExists 测试
     *
     * @author cakin
     * @date 2020/11/15
     */
    @Test
    public void testSelectObjs() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
//        queryWrapper.in("id", 1, 2, 3);
        queryWrapper.inSql("id", "select id from user where id <= 3");
        List<Object> objects = userMapper.selectObjs(queryWrapper); // 返回值是Object列表
        objects.forEach(System.out::println);
    }

    /**
     * 功能描述：or、and 测试
     *
     * @author cakin
     * @date 2020/11/15
     */
    @Test
    public void testUpdate1() {
        // 修改值
        User user = new User();
        user.setAge(99);
        user.setName("Andy");
        // 修改条件
        UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
        userUpdateWrapper
                .like("name", "h")
                .or()
                .between("age", 20, 30);
        int result = userMapper.update(user, userUpdateWrapper);
        System.out.println(result);
    }


    /**
     * 功能描述：lambda表达式 测试
     *
     * @author cakin
     * @date 2020/11/15
     */
    @Test
    public void testUpdate2() {
        //修改值
        User user = new User();
        user.setAge(99);
        user.setName("Andy");
        //修改条件
        UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
        userUpdateWrapper
                .like("name", "n")
                .or(i -> i.like("name", "a").eq("age", 20));
        int result = userMapper.update(user, userUpdateWrapper);
    }

    /**
     * 功能描述：orderBy、orderByDesc、orderByAsc 测试
     *
     * @author cakin
     * @date 2020/11/15
     */
    @Test
    public void testSelectListOrderBy() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("age", "id");
        List<User> users = userMapper.selectList(queryWrapper);
        users.forEach(System.out::println);
    }

    /**
     * 功能描述：set、setSql 测试
     *
     * @author cakin
     * @date 2020/11/15
     */
    @Test
    public void testUpdateSet() {
        // 修改值
        User user = new User();
        user.setAge(60);
        // 修改条件
        UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
        userUpdateWrapper
                .like("name", "h")
                .set("name", "Peter") // 除了可以查询还可以使用set设置修改的字段
                .setSql(" email = '123@qq.com'"); // 可以有子查询
        int result = userMapper.update(user, userUpdateWrapper);
    }
}
