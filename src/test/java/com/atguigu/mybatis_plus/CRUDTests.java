package com.atguigu.mybatis_plus;

import com.atguigu.mybatis_plus.entity.Product;
import com.atguigu.mybatis_plus.entity.User;
import com.atguigu.mybatis_plus.mapper.ProductMapper;
import com.atguigu.mybatis_plus.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @className: CRUDTests
 * @description: CRUD测试
 * @date: 2020/11/14
 * @author: cakin
 */
@SpringBootTest
public class CRUDTests {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProductMapper productMapper;

    @Test
    public void testInsert() {

        User user = new User();
        user.setName("cakin");
        user.setEmail("798103175@qq.com");
        user.setAge(18);
//        user.setCreateTime(new Date());
//        user.setUpdateTime(new Date());

        //返回值：影响的行数
        int result = userMapper.insert(user);
        System.out.println("影响的行数：" + result); // 影响的行数
        System.out.println("user id：" + user.getId()); // id自动回填
    }

    @Test
    public void testUpdateById() {

        User user = new User();
        user.setId(1244892079889334273L);
        user.setAge(28);
        user.setName("Annie");
//        user.setUpdateTime(new Date());

        int result = userMapper.updateById(user);
        System.out.println("影响的行数：" + result);
    }


    @Test
    public void testConcurrentUpdate() {
        // 1 小李获取数据
        Product p1 = productMapper.selectById(1L);
        System.out.println("小李取出的价格：" + p1.getPrice());

        // 2 小王获取数据
        Product p2 = productMapper.selectById(1L);
        System.out.println("小王取出的价格：" + p2.getPrice());

        // 3 小李加了50，存入数据库
        p1.setPrice(p1.getPrice() + 50);
        productMapper.updateById(p1);

        // 4 小王减了30员，存入数据库
        p2.setPrice(p2.getPrice() - 30);
        int result = productMapper.updateById(p2);
        if (result == 0) {
            System.out.println("小王更新失败");
            //发起重试
            p2 = productMapper.selectById(1L);
            p2.setPrice(p2.getPrice() - 30);
            productMapper.updateById(p2);
        }

        // 最后的结果
        // 用户看到的商品价格
        Product p3 = productMapper.selectById(1L);
        System.out.println("最后的结果：" + p3.getPrice());
    }


    /**
     * 功能描述：通过多个id批量查询
     *
     * @author cakin
     * @date 2020/11/14
     */
    @Test
    public void testSelectBatchIds() {
        List<User> users = userMapper.selectBatchIds(Arrays.asList(1, 2, 3));
        users.forEach(System.out::println);
    }

    /**
     * 功能描述：通过map封装查询条件
     *
     * @author cakin
     * @date 2020/11/14
     * @description: map中的key对应数据库中的列名。如：数据库user_id，实体类是userId，这时map的key需要填写user_id
     */
    @Test
    public void testSelectByMap() {
        HashMap<String, Object> map = new HashMap<>();
        // key 是数据表的字段，不是实体类的属性
        map.put("name", "cakin");
        map.put("age", 18);
        List<User> users = userMapper.selectByMap(map);
        users.forEach(System.out::println);
    }

    /**
     * 功能描述：分页查询
     *
     * @author cakin
     * @date 2020/11/14
     */
    @Test
    public void testSelectPage() {
        // 第1个参数：查询第几页 第2个参数：每页几条数据
        Page<User> page = new Page<>(1, 5);
        // 第1个参数：分页参数 第2个参数：条件查询包装参数
        Page<User> pageParam = userMapper.selectPage(page, null);

        // 下面两条语句等价，查询结果相同
        // List<User> records = page.getRecords();
        List<User> records = pageParam.getRecords();

        records.forEach(System.out::println);
        System.out.println(pageParam.getPages()); // 总页数
        System.out.println(pageParam.getTotal()); // 总记录数
        System.out.println(pageParam.getCurrent()); // 当前页码
        System.out.println(pageParam.getSize()); // 每页记录数
        System.out.println(pageParam.hasNext()); // 是否有下一页
        System.out.println(pageParam.hasPrevious()); // 是否有上一页
    }

    /**
     * 功能描述：返回指定的列
     *
     * @author cakin
     * @date 2020/11/14
     */
    @Test
    public void testSelectMapsPage() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        // 返回指定列，这里是数据库的字段，而不是实体类的属性
        queryWrapper.select("id", "name");
        Page<Map<String, Object>> page = new Page<>(1, 5);
        // 第1个参数：分页参数 第2个参数：条件查询包装参数——指定了返回哪些列
        Page<Map<String, Object>> pageParam = userMapper.selectMapsPage(page, queryWrapper);
        List<Map<String, Object>> records = pageParam.getRecords();
        records.forEach(System.out::println);
        System.out.println(pageParam.getPages()); // 总页数
        System.out.println(pageParam.getTotal()); // 总记录数
        System.out.println(pageParam.getCurrent()); // 当前页码
        System.out.println(pageParam.getSize()); // 每页记录数
        System.out.println(pageParam.hasNext()); // 是否有下一页
        System.out.println(pageParam.hasPrevious()); // 是否有上一页
    }

    /**
     * 功能描述：根据Id进行删除
     *
     * @author cakin
     * @date 2020/11/14
     */
    @Test
    public void testDeleteById() {
        int result = userMapper.deleteById(4L);
        System.out.println("删除了" + result + "行");
    }

    /**
     * 功能描述：根据Id列表进行批量删除
     *
     * @author cakin
     * @date 2020/11/14
     */
    @Test
    public void testDeleteBatchIds() {
        int result = userMapper.deleteBatchIds(Arrays.asList(1, 2, 3));
        System.out.println("删除了" + result + "行");
    }

    /**
     * 功能描述：通过map进行简单条件删除
     *
     * @author cakin
     * @date 2020/11/14
     */
    @Test
    public void testDeleteByMap() {
        // 以map的形式封装删除条件
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "cakin");
        map.put("age", 18);
        int result = userMapper.deleteByMap(map);
        System.out.println("删除了" + result + "行");
    }

    /**
     * 功能描述：逻辑删除
     *
     * @author cakin
     * @date 2020/11/14
     */
    @Test
    public void testLogicDeleteById() {
        int result = userMapper.deleteById(1L);
        System.out.println("删除了" + result + "行");
    }

    /**
     * 功能描述：测试逻辑删除后的查询
     *
     * @author cakin
     * @date 2020/11/14
     */
    @Test
    void testLogicSelectList() {
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }
}
