package com.atguigu.mybatis_plus.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @className: MyMetaObjectHandler
 * @description: 实现元对象处理器接口
 * @date: 2020/11/14
 * @author: cakin
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    /**
     * 功能描述：添加数据时处理
     *
     * @param metaObject 元对象
     * @author cakin
     * @date 2020/11/14
     * @description: 当添加一个对象时，如果一个对象中有 createTime 和 updateTime，会自动填充这两个字段，值为 new Date()
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        System.out.println("insertFill 。。。。。。");
        this.setFieldValByName("createTime", new Date(), metaObject);
        this.setFieldValByName("updateTime", new Date(), metaObject);
    }

    /**
     * 功能描述：更新数据时处理
     *
     * @param metaObject 元对象
     * @author cakin
     * @date 2020/11/14
     * @description: 当更新一个对象时，如果一个对象中有 updateTime，会自动填充这个字段，值为 new Date()
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        System.out.println("updateFill 。。。。。。");
        this.setFieldValByName("updateTime", new Date(), metaObject);
    }
}
