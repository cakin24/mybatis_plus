package com.atguigu.mybatis_plus.entity;

import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

/**
 * @className: Product
 * @description: 产品
 * @date: 2020/11/14
 * @author: cakin
 */
@Data
public class Product {
    private Long id;
    private String name;
    private Integer price;
    /**
     * 乐观锁的版本号
     */
    @Version
    private Integer version;
}
