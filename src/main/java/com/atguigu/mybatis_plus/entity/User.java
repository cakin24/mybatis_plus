package com.atguigu.mybatis_plus.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.util.Date;

/**
 * @author helen
 * @since 2020/3/31
 */
@Data
public class User {

//    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    private Integer age;
    private String email;
    // 自动填充注解
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    // 自动填充注解
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    // 该注解用于逻辑删除
    @TableLogic
    private Integer deleted;
}
