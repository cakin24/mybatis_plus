package com.atguigu.mybatis_plus.mapper;

import com.atguigu.mybatis_plus.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @className: ProductMapper
 * @description: 产品mapper
 * @date: 2020/11/14
 * @author: cakin
 */
@Repository
public interface ProductMapper extends BaseMapper<Product> {
}
