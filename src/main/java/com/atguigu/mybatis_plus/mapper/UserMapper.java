package com.atguigu.mybatis_plus.mapper;

import com.atguigu.mybatis_plus.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author helen
 * @since 2020/3/31
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
